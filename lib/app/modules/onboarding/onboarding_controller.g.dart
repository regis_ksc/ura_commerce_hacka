// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'onboarding_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$OnboardingController on _OnboardingControllerBase, Store {
  Computed<String> _$bottomFirstMessageComputed;

  @override
  String get bottomFirstMessage => (_$bottomFirstMessageComputed ??=
          Computed<String>(() => super.bottomFirstMessage,
              name: '_OnboardingControllerBase.bottomFirstMessage'))
      .value;
  Computed<String> _$bottomSecondMessageComputed;

  @override
  String get bottomSecondMessage => (_$bottomSecondMessageComputed ??=
          Computed<String>(() => super.bottomSecondMessage,
              name: '_OnboardingControllerBase.bottomSecondMessage'))
      .value;
  Computed<String> _$buttonMessageComputed;

  @override
  String get buttonMessage =>
      (_$buttonMessageComputed ??= Computed<String>(() => super.buttonMessage,
              name: '_OnboardingControllerBase.buttonMessage'))
          .value;

  final _$isInitialAtom = Atom(name: '_OnboardingControllerBase.isInitial');

  @override
  bool get isInitial {
    _$isInitialAtom.reportRead();
    return super.isInitial;
  }

  @override
  set isInitial(bool value) {
    _$isInitialAtom.reportWrite(value, super.isInitial, () {
      super.isInitial = value;
    });
  }

  final _$isLoginAtom = Atom(name: '_OnboardingControllerBase.isLogin');

  @override
  bool get isLogin {
    _$isLoginAtom.reportRead();
    return super.isLogin;
  }

  @override
  set isLogin(bool value) {
    _$isLoginAtom.reportWrite(value, super.isLogin, () {
      super.isLogin = value;
    });
  }

  final _$_OnboardingControllerBaseActionController =
      ActionController(name: '_OnboardingControllerBase');

  @override
  dynamic setIsInitial(bool value) {
    final _$actionInfo = _$_OnboardingControllerBaseActionController
        .startAction(name: '_OnboardingControllerBase.setIsInitial');
    try {
      return super.setIsInitial(value);
    } finally {
      _$_OnboardingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setIsLogin(bool value) {
    final _$actionInfo = _$_OnboardingControllerBaseActionController
        .startAction(name: '_OnboardingControllerBase.setIsLogin');
    try {
      return super.setIsLogin(value);
    } finally {
      _$_OnboardingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isInitial: ${isInitial},
isLogin: ${isLogin},
bottomFirstMessage: ${bottomFirstMessage},
bottomSecondMessage: ${bottomSecondMessage},
buttonMessage: ${buttonMessage}
    ''';
  }
}
