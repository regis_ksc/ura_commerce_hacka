import 'components/onboarding_content/components/register/register_controller.dart';
import 'components/onboarding_content/components/login/login_controller.dart';
import 'onboarding_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import 'onboarding_page.dart';

class OnboardingModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => RegisterController()),
        Bind((i) => LoginController()),
        Bind((i) => OnboardingController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => OnboardingPage()),
      ];

  static Inject get to => Inject<OnboardingModule>.of();
}
