import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/header/header_widget.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_content/onboarding_content.dart';
import '../../app_controller.dart';
import 'onboarding_controller.dart';

class OnboardingPage extends KFDrawerContent {
  final String title;
  OnboardingPage({Key key, this.title = "Onboarding"});

  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends ModularState<OnboardingPage, OnboardingController>
    with SingleTickerProviderStateMixin {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        body: LayoutBuilder(
          builder: (context, constraints) {
            final double height = constraints.biggest.height;
            final double width = constraints.biggest.width;
            return Stack(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/uberaba.jpg'),
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
                AnimatedContainer(
                  duration: Duration(milliseconds: 800),
                  height: height,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        controller.isInitial ? Cores.amarelo75 : Cores.azul70,
                        controller.isInitial ? Cores.amarelo50 : Cores.azul40,
                        controller.isInitial ? Cores.amarelo75 : Cores.azul70,
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                ),
                AnimatedContainer(
                  duration: Duration(milliseconds: 200),
                  margin: EdgeInsets.only(
                    top: Modular.get<AppController>().keyboardIsShown
                        ? (controller.isLogin ? height * 0.05 : height * 0.00)
                        : height * 0.15,
                    bottom: Modular.get<AppController>().keyboardIsShown ? height * 0.25 : height * 0.05,
                  ),
                  child: LayoutBuilder(builder: (_, constraints) {
                    return Column(
                      children: <Widget>[
                        Container(
                          height: constraints.biggest.height * 0.2,
                          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                          child: HeaderWidget(),
                        ),
                        SizedBox(height: constraints.biggest.height * 0.02),
                        SingleChildScrollView(
                          child: Container(
                            height: constraints.biggest.height * 0.78,
                            child: OnboardingContent(),
                          ),
                        ),
                      ],
                    );
                  }),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
