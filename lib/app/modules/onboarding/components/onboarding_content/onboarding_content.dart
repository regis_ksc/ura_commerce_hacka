import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_content/components/initial/initial_widget.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_content/components/login/login_widget.dart';

import '../../../../app_controller.dart';
import '../../onboarding_controller.dart';
import 'components/onboarding_button/onboarding_button.dart';
import 'components/register/register_widget.dart';

class OnboardingContent extends StatelessWidget {
  final double height;
  final double width;

  OnboardingContent({Key key, this.height, this.width}) : super(key: key);

  final controller = Modular.get<OnboardingController>();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        final double height = constraints.biggest.height;
        final double width = constraints.biggest.width;
        return Container(
          margin: EdgeInsets.fromLTRB(width * 0.05, 0, width * 0.05, 0),
          child: Observer(
            builder: (_) {
              return Column(
                children: <Widget>[
                  Container(
                    height: (Modular.get<AppController>().keyboardIsShown && controller.isLogin)
                        ? height * 0.4
                        : height * 0.7,
                    child: AnimatedCrossFade(
                      crossFadeState: controller.isInitial ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                      duration: Duration(milliseconds: 800),
                      firstCurve: Curves.easeInOut,
                      secondCurve: Curves.easeInOut,
                      firstChild: IgnorePointer(
                        ignoring: !controller.isInitial,
                        child: InitialWidget(
                          height: height,
                          width: width,
                        ),
                      ),
                      secondChild: Visibility(
                        visible: controller.isLogin,
                        child: LoginWidget(),
                        replacement: RegisterWidget(),
                      ),
                    ),
                  ),
                  AnimatedOpacity(
                    opacity: controller.isInitial ? 0 : 1,
                    duration: Duration(milliseconds: 200),
                    curve: Curves.easeInOut,
                    child: Container(
                      height: height * 0.3,
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(vertical: height * 0.03),
                            child: OnboardingButton(
                              width: MediaQuery.of(context).size.width,
                              bgColor: Colors.white,
                              label: controller.buttonMessage,
                              onTap: () {
                                //
                                if (!controller.isInitial && controller.isLogin) {
                                  // LOGICA DE LOGAR
                                } else if (!controller.isInitial && !controller.isLogin) {
                                  // LOGICA DE CADASTRAR
                                }
                                Modular.to.pushReplacementNamed('/dashboard');
                              },
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                controller.bottomFirstMessage,
                                style: Fontes.nunito.copyWith(
                                  color: Cores.amarelo75,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  if (controller.isLogin) {
                                    controller.setIsLogin(false);
                                  } else {
                                    controller.setIsLogin(true);
                                  }
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Cores.amarelo100.withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Text(
                                    controller.bottomSecondMessage,
                                    style: Fontes.nunito.copyWith(
                                      color: Cores.vermelho100,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        );
      },
    );
  }
}
