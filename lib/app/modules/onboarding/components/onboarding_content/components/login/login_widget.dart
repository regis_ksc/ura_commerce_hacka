import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_component/login/esqueceu_senha/esqueceu_senha.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_content/components/input_field/input_field_widget.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_content/components/onboarding_button/onboarding_button.dart';

import '../../../../../../app_controller.dart';
import 'login_controller.dart';

class LoginWidget extends StatelessWidget {
  final controller = Modular.get<LoginController>();
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (_, constraints) {
      final double height = constraints.biggest.height;
      final double width = constraints.biggest.width;
      return KeyboardAvoider(
        autoScroll: true,
        child: Column(
          children: <Widget>[
            Container(height: Modular.get<AppController>().keyboardIsShown ? height * 0.012 : height * 0.25),
            InputFieldWidget(
              label: "E-mail",
              onChanged: (String text) {
                controller.setEmail(text);
                print(controller.email);
              },
            ),
            Container(height: height * 0.12),
            InputFieldWidget(
              label: "Senha",
              obscure: true,
              onChanged: (String text) {
                controller.setEmail(text);
                print(controller.email);
              },
              icon: InkWell(
                onTap: () {},
                child: Icon(
                  Icons.visibility,
                  color: Colors.white,
                  size: constraints.biggest.width * 0.08,
                ),
              ),
            ),
            EsqueceuSenha(
              height: height,
              width: width,
            ),
          ],
        ),
      );
    });
  }
}
