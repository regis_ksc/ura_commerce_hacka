import 'package:mobx/mobx.dart';

part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  @observable
  String email;
  @action
  setEmail(String value) => email = value;

  @observable
  String password;
  @action
  setPassword(String value) => password = value;

  bool get allDataIsFilled {
    if (email != null && email != '') {
      if (password != null && password != '') {
        if (password != null && password != '') {
          return true;
        }
      }
    }
    return false;
  }
}
