import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class InputFieldWidget extends StatelessWidget {
  final String label;
  final bool obscure;
  final Widget icon;
  final Function onChanged;
  InputFieldWidget({Key key, this.label, this.obscure, this.icon, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final borderProps = OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
      borderSide: BorderSide(color: Colors.white),
    );
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.2),
        borderRadius: BorderRadius.circular(30),
      ),
      child: TextFormField(
        onChanged: onChanged,
        style: Fontes.nunito.copyWith(
          fontSize: 16,
          color: Cores.azul75,
          fontWeight: FontWeight.bold,
          letterSpacing: 1,
        ),
        obscureText: obscure ?? false,
        decoration: InputDecoration(
          labelText: label,
          labelStyle: Fontes.nunito.copyWith(fontSize: 18, color: Colors.white),
          focusedBorder: borderProps,
          enabledBorder: borderProps,
          suffixIcon: icon,
        ),
      ),
    );
  }
}
