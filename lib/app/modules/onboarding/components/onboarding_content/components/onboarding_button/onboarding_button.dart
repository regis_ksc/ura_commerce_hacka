import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/button/button_widget.dart';

class OnboardingButton extends StatelessWidget {
  final String label;
  final double width;
  final Function onTap;
  final Color bgColor;
  final Color labelColor;

  const OnboardingButton({Key key, this.label, this.width, this.onTap, this.bgColor, this.labelColor})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: width * 0.15),
        child: ButtonWidget(
          backgroundColor: bgColor,
          borderRadius: 25,
          child: Text(
            label,
            style: Fontes.nunito.copyWith(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: labelColor,
              letterSpacing: 0.5,
            ),
          ),
        ),
      ),
    );
  }
}
