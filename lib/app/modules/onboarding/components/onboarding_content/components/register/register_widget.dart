import 'package:flutter/material.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_content/components/input_field/input_field_widget.dart';

class RegisterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: 5),
          InputFieldWidget(
            label: "Nome",
          ),
          Spacer(),
          Row(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: InputFieldWidget(
                  label: "CPF",
                ),
              ),
              Spacer(),
              Expanded(
                flex: 8,
                child: InputFieldWidget(
                  label: "Telefone",
                ),
              ),
            ],
          ),
          Spacer(),
          InputFieldWidget(
            label: "E-mail",
          ),
          Spacer(),
          InputFieldWidget(
            label: "Senha",
          ),
          Spacer(),
          InputFieldWidget(
            label: "Confirmar Senha",
          ),
        ],
      ),
    );
  }
}
