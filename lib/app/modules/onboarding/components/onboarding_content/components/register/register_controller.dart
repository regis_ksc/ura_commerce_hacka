import 'package:mobx/mobx.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _$RegisterController;

abstract class _RegisterControllerBase with Store {
  @observable
  String name;
  @action
  setName(String value) => name = value;

  @observable
  String cpf;
  @action
  setCpf(String value) => cpf = value;

  @observable
  String telephone;
  @action
  setTelephone(String value) => telephone = value;

  @observable
  String email;
  @action
  setEmail(String value) => email = value;

  @observable
  String password;
  @action
  setPasssword(String value) => password = value;

  @observable
  String confirmPassword;
  @action
  setConfirmPassword(String value) => confirmPassword = value;

  @computed

  /// Cada verificação dessas checa se um dado é nulo
  bool get allDataIsFilled {
    if (name != null && name != '') {
      if (cpf != null && cpf != '') {
        if (telephone != null && telephone != '') {
          if (email != null && email != '') {
            if (password != null && password != '') {
              if (password != null && password != '') {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  @computed
  bool get passwordsEqual {
    if (password.length == confirmPassword.length) {
      if (password == confirmPassword) {
        return true;
      }
    }
    return false;
  }

  @action
  register() {
    if (passwordsEqual && allDataIsFilled) {}
  }
}
