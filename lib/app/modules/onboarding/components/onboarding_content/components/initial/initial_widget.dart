import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/components/onboarding_content/components/onboarding_button/onboarding_button.dart';

import '../onboarding_button/onboarding_button.dart';

import '../../../../onboarding_controller.dart';

class InitialWidget extends StatelessWidget {
  final double width;
  final double height;

  InitialWidget({Key key, this.width, this.height}) : super(key: key);

  final controller = Modular.get<OnboardingController>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: height * 0.4),
          OnboardingButton(
            label: "ENTRAR",
            bgColor: Colors.white,
            labelColor: Cores.azul100,
            onTap: () {
              if (controller.isInitial != false) {
                controller.setIsInitial(false);
              }
              controller.setIsLogin(true);
              print("isLogin: ${controller.isLogin}");
              print("isInitial: ${controller.isInitial}");
            },
            width: width,
          ),
          SizedBox(height: height * 0.05),
          OnboardingButton(
            label: "CRIAR CONTA",
            bgColor: Cores.azul100,
            labelColor: Colors.white,
            width: width,
            onTap: () {
              if (controller.isInitial != false) {
                controller.setIsInitial(false);
              }
              controller.setIsLogin(false);
              print("isLogin: ${controller.isLogin}");
              print("isInitial: ${controller.isInitial}");
            },
          ),
        ],
      ),
    );
  }
}
