import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

import '../../onboarding_controller.dart';

class HeaderWidget extends StatelessWidget {
  final controller = Modular.get<OnboardingController>();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 70,
          child: Container(
            child: Hero(
              tag: 'dino',
              child: Image.asset(
                "assets/logos/dinossauro.png",
                color: controller.isInitial ? Colors.white : Cores.amarelo75,
                fit: BoxFit.fitHeight,
                width: 90,
              ),
            ),
          ),
        ),
        Expanded(
          flex: 30,
          child: Container(
            child: Text(
              "URA COMMERCE",
              style: Fontes.nunito.copyWith(
                fontSize: 30,
                color: controller.isInitial ? Cores.azul100 : Cores.amarelo75,
                letterSpacing: 0.5,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
