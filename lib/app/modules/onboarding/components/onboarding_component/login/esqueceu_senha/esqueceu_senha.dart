import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';

class EsqueceuSenha extends StatelessWidget {
  final double width;
  final double height;

  const EsqueceuSenha({Key key, this.width, this.height}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      margin: EdgeInsets.only(right: width * 0.05),
      padding: EdgeInsets.symmetric(vertical: height * 0.01),
      child: Text(
        "Esqueceu sua senha?",
        style: Fontes.nunito.copyWith(
          color: Cores.amarelo75,
          fontWeight: FontWeight.w900,
          fontSize: 14,
        ),
      ),
    );
  }
}
