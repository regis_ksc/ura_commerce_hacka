import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'onboarding_controller.g.dart';

class OnboardingController = _OnboardingControllerBase with _$OnboardingController;

abstract class _OnboardingControllerBase extends Disposable with Store {
  dispose() {}
  @observable
  bool isInitial = true;
  @action
  setIsInitial(bool value) => isInitial = value;

  @observable
  bool isLogin = false;
  @action
  setIsLogin(bool value) => isLogin = value;

  @computed
  String get bottomFirstMessage {
    if (isLogin) {
      return "Ainda não tem conta?";
    } else {
      return "Já possui conta?";
    }
  }

  @computed
  String get bottomSecondMessage {
    if (isLogin) {
      return " Cadastre-se!";
    } else {
      return " Fazer login!";
    }
  }

  @computed
  String get buttonMessage {
    if (isLogin) {
      return "ENTRAR";
    } else {
      return "CADASTRAR";
    }
  }
}
