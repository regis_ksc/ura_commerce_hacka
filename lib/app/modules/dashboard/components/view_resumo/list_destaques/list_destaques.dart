import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_retangular/card_retangular.dart';

class ListDestaques extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        CardRetangular(
          caminho: "assets/icons/lojas/tecno.png",
          proporcao: 2,
          onTap: () {
            Modular.to.pushNamed('/dashboard/shop');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/lojas/mercadin.png",
          proporcao: 2,
          onTap: () {
            Modular.to.pushNamed('/dashboard/shop');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/lojas/pett.png",
          proporcao: 2,
          onTap: () {
            Modular.to.pushNamed('/dashboard/shop');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/lojas/tecno.png",
          proporcao: 2,
          onTap: () {
            Modular.to.pushNamed('/dashboard/shop');
          },
        ),
      ],
    );
  }
}
