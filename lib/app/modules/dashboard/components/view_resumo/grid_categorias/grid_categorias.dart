import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_retangular/card_retangular.dart';

class GridCategorias extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(child: PrimeiraLinha()),
        Expanded(child: SegundaLinha()),
      ],
    );
  }
}

class PrimeiraLinha extends StatelessWidget {
  const PrimeiraLinha({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        CardRetangular(
          caminho: "assets/icons/categorias/moda_feminina.png",
          label: "Moda feminina",
          onTap: () {
            Modular.to.pushNamed('/dashboard/categories');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/categorias/moda_masculina.png",
          label: "Moda masculina",
          onTap: () {
            Modular.to.pushNamed('/dashboard/categories');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/categorias/mercado.png",
          label: "Mercado",
          onTap: () {
            Modular.to.pushNamed('/dashboard/categories');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/categorias/esportivos.png",
          label: "Artigos Esportivos",
          onTap: () {
            Modular.to.pushNamed('/dashboard/categories');
          },
        ),
      ],
    );
  }
}

class SegundaLinha extends StatelessWidget {
  const SegundaLinha({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        CardRetangular(
          caminho: "assets/icons/categorias/petshop.png",
          label: "Petshop",
          onTap: () {
            Modular.to.pushNamed('/dashboard/categories');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/categorias/tecnologia.png",
          label: "Tecnologia",
          onTap: () {
            Modular.to.pushNamed('/dashboard/categories');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/categorias/farmacia.png",
          label: "Farmacia",
          onTap: () {
            Modular.to.pushNamed('/dashboard/categories');
          },
        ),
        CardRetangular(
          caminho: "assets/icons/categorias/mais.png",
          label: "Ver mais",
          onTap: () {
            showBottomSheet(
              context: context,
              elevation: 50,
              builder: (context) {
                return GestureDetector(
                  onTap: () {
                    // Modular.to.pop();
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.75,
                    decoration: BoxDecoration(
                      color: Cores.azul100.withOpacity(0.10),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15),
                      ),
                    ),
                    padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.03,
                      horizontal: MediaQuery.of(context).size.width * 0.03,
                    ),
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            flex: 5,
                            child: Row(
                              children: <Widget>[
                                Spacer(flex: 90),
                                Expanded(
                                  flex: 10,
                                  child: Center(
                                    child: IconButton(
                                      onPressed: () {
                                        Modular.to.pop();
                                      },
                                      icon: Icon(Icons.clear, color: Colors.redAccent[400]),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 10,
                            child: Container(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                "Outras categorias para você",
                                style: Fontes.nunito.copyWith(
                                  color: Cores.azul100,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w900,
                                ),
                              ),
                            ),
                          ),
                          Spacer(flex: 3),
                          Expanded(
                            flex: 22,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                CardRetangular(
                                  caminho: "assets/icons/categorias/padaria.png",
                                  label: "Padaria",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/restaurantes.png",
                                  label: "Restaurante",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/calcados.png",
                                  label: "Calçados",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/infantil.png",
                                  label: "Infantil",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                              ],
                            ),
                          ),
                          Spacer(flex: 3),
                          Expanded(
                            flex: 22,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                CardRetangular(
                                  caminho: "assets/icons/categorias/tecnologia.png",
                                  label: "Tecnologia",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/petshop.png",
                                  label: "Petshop",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/mercado.png",
                                  label: "Mercado",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/farmacia.png",
                                  label: "Farmacia",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                              ],
                            ),
                          ),
                          Spacer(flex: 3),
                          Expanded(
                            flex: 22,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                CardRetangular(
                                  caminho: "assets/icons/categorias/farmacia.png",
                                  label: "Farmacia",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/esportivos.png",
                                  label: "Artigos esportivos",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/moda_masculina.png",
                                  label: "Moda Masculina",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                                CardRetangular(
                                  caminho: "assets/icons/categorias/moda_feminina.png",
                                  label: "Moda Feminina",
                                  onTap: () {
                                    Modular.to.pop();
                                    Modular.to.pushNamed('/dashboard/categories');
                                  },
                                ),
                              ],
                            ),
                          ),
                          Spacer(flex: 10),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ],
    );
  }
}
