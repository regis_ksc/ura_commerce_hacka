import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/anuncio_footer/anuncio_footer.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/view_resumo/list_destaques/list_destaques.dart';

import 'grid_categorias/grid_categorias.dart';

class ViewResumo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                child: Text(
                  "Destaques",
                  style: Fontes.nunito.copyWith(
                    color: Cores.azul100,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(child: ListDestaques()),
              ),
              Container(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                child: Text(
                  "Categorias",
                  style: Fontes.nunito.copyWith(
                    color: Cores.azul100,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: Container(
                  child: GridCategorias(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
