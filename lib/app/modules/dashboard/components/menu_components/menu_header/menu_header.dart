import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class MenuHeader extends StatelessWidget {
  final String caminhoImg;
  final String userName;

  const MenuHeader({Key key, this.caminhoImg, this.userName}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width * 0.6,
      margin: EdgeInsets.only(
        left: MediaQuery.of(context).size.width * 0.02,
        top: MediaQuery.of(context).size.width * 0.02,
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 40,
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: Cores.amarelo100),
                borderRadius: BorderRadius.circular(50),
              ),
              margin: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.02,
                vertical: MediaQuery.of(context).size.width * 0.02,
              ),
              child: Visibility(
                visible: caminhoImg != null,
                replacement: Container(),
                child: Center(
                  child: Image.asset(caminhoImg ?? ""),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 60,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 55,
                    child: Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.05),
                      child: Text(
                        "Olá${userName == null ? "!" : ""}",
                        style: Fontes.nunito.copyWith(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 45,
                    child: Container(
                      child: Text(
                        userName ?? "",
                        style: Fontes.nunito.copyWith(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Cores.amarelo75,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
