import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'sales_controller.dart';

class SalesPage extends KFDrawerContent {
  final String title;
  SalesPage({Key key, this.title = "Sales"});

  @override
  _SalesPageState createState() => _SalesPageState();
}

class _SalesPageState extends ModularState<SalesPage, SalesController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
