import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'components/profile_components.dart';
import 'profile_controller.dart';
import 'package:flutter/src/material/raised_button.dart';

class ProfilePage extends KFDrawerContent {
  final String title;
  ProfilePage({Key key, this.title = "Profile"});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends ModularState<ProfilePage, ProfileController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return SafeArea(
      child: WillPopScope(
        onWillPop: () {
          widget.onMenuPressed.call();
          return;
        },
        child: Scaffold(
          body: Container(
            padding: EdgeInsets.only(top: height * 0.02),
            color: Cores.azul100,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Cores.amarelo100,
                    size: 40,
                  ),
                  onPressed: () {
                    widget.onMenuPressed.call();
                  },
                  alignment: Alignment.topLeft,
                ),
                Spacer(),
                Container(
                  padding: EdgeInsets.only(left: width * 0.05),
                  child: Text(
                    "Minha conta",
                    textAlign: TextAlign.left,
                    style: Fontes.nunito.copyWith(
                      color: Cores.amarelo100,
                      fontSize: 35,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.7,
                    ),
                  ),
                ),
                Spacer(),
                UserWidget(dado: "Dados pessoais"),
                SizedBox(height: 10),
                UserWidget(dado: "Endereços"),
                SizedBox(height: 10),
                UserWidget(dado: "Formas de pagamento"),
                Spacer(flex: 3),
                Container(
                  child: Hero(
                    tag: 'dino',
                    child: Image.asset(
                      'assets/logos/dinossauro.png',
                      color: Cores.amarelo100,
                      alignment: Alignment.center,
                      fit: BoxFit.fitWidth,
                      width: 90,
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
