import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'profile_controller.g.dart';

class ProfileController = _ProfileControllerBase with _$ProfileController;

abstract class _ProfileControllerBase extends Disposable with Store {
  dispose() {}
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
