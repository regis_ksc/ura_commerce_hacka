import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class UserWidget extends StatelessWidget {
  final String dado;

  const UserWidget({Key key, this.dado}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.only(left: width * 0.05),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              dado,
              textAlign: TextAlign.left,
              style: Fontes.nunito
                  .copyWith(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold, letterSpacing: 0.0),
            ),
          ),
          Expanded(
            flex: 1,
            child: IconButton(
              icon: Icon(
                Icons.arrow_forward_ios,
                color: Cores.amarelo100,
                size: 40,
              ),
              onPressed: () {},
              alignment: Alignment.centerRight,
            ),
          ),
        ],
      ),
    );
  }
}
