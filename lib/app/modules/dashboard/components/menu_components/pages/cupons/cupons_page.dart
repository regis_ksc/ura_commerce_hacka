import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'cupons_controller.dart';

class CuponsPage extends KFDrawerContent {
  final String title;
  CuponsPage({Key key, this.title = "Cupons"});

  @override
  _CuponsPageState createState() => _CuponsPageState();
}

class _CuponsPageState extends ModularState<CuponsPage, CuponsController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
