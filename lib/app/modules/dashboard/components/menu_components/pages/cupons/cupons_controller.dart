import 'package:mobx/mobx.dart';

part 'cupons_controller.g.dart';

class CuponsController = _CuponsControllerBase with _$CuponsController;

abstract class _CuponsControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
