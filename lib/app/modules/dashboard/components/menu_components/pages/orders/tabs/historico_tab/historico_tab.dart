import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/components/card_pedido/card_pedido.dart';

class HistoricoTab extends StatelessWidget {
  final int itemCount;

  const HistoricoTab({Key key, this.itemCount = 0}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView.builder(
        itemCount: itemCount,
        itemBuilder: (context, index) {
          return CardPedido();
        },
      ),
    );
  }
}
