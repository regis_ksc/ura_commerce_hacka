import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class QuantidadeDeProduto extends StatelessWidget {
  final String produto;
  final int quantidade;

  const QuantidadeDeProduto({
    Key key,
    this.produto,
    this.quantidade = 1,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(top: height * 0.01),
      height: width * 0.08,
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 10,
              child: Card(
                color: Cores.azul40,
                child: SizedBox.expand(
                  child: Center(
                    child: Text(
                      quantidade.toString(),
                      style: Fontes.montserrat.copyWith(
                        fontWeight: FontWeight.w900,
                        fontSize: 12,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              )),
          Spacer(flex: 15),
          Expanded(
              flex: 65,
              child: Container(
                child: Text(
                  produto ?? "",
                  style: Fontes.nunito.copyWith(
                    color: Cores.azul75,
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )),
          Spacer(flex: 10),
        ],
      ),
    );
  }
}
