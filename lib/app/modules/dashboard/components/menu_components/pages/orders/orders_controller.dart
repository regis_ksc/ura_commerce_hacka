import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'orders_controller.g.dart';

class OrdersController = _OrdersControllerBase with _$OrdersController;

abstract class _OrdersControllerBase extends Disposable with Store {
  dispose() {}
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
