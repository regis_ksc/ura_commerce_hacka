import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/button/button_widget.dart';

class BotoesPedido extends StatelessWidget {
  final double width;
  final double height;

  const BotoesPedido({Key key, this.width, this.height}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: width * 0.01, vertical: height * 0.005),
            child: ButtonWidget(
              backgroundColor: Cores.amarelo100.withOpacity(0.5),
              onTap: () {},
              child: Text(
                "Detalhes",
                style: Fontes.montserrat.copyWith(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: width * 0.01, vertical: height * 0.005),
            child: ButtonWidget(
              backgroundColor: Cores.amarelo100.withOpacity(0.5),
              onTap: () {},
              child: Text(
                "Ajuda",
                style: Fontes.montserrat.copyWith(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
