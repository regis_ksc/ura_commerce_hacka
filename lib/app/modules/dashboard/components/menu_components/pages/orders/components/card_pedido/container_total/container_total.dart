import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class ContainerTotal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Total: ",
            style: Fontes.montserrat.copyWith(
              color: Cores.azul100,
              fontWeight: FontWeight.w500,
              fontSize: 12,
            ),
          ),
          Text(
            "R\$ 98,10",
            style: Fontes.montserrat.copyWith(
              color: Cores.azul100,
              fontWeight: FontWeight.w900,
              fontSize: 13,
            ),
          ),
        ],
      ),
    );
  }
}
