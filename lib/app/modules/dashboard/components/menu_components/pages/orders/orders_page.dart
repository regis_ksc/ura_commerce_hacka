import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_expandido/card_expandido.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/header_interno/header_interno.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/tabs/em_andamento_tab/em_andamento_tab.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/tabs/historico_tab/historico_tab.dart';
import 'orders_controller.dart';

class OrdersPage extends KFDrawerContent {
  final String title;
  final bool veioDeVenda;
  OrdersPage({Key key, this.title = "Orders", this.veioDeVenda = false});

  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends ModularState<OrdersPage, OrdersController> with TickerProviderStateMixin {
  //use 'controller' variable to access controller

  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      vsync: this,
      initialIndex: 0,
      length: 2,
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        widget.onMenuPressed.call();
      },
      child: SafeArea(
        child: DefaultTabController(
          length: 2,
          initialIndex: 0,
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            resizeToAvoidBottomPadding: false,
            body: Column(
              children: <Widget>[
                HeaderInterno(
                  iconeEsquerdo: !widget.veioDeVenda ? Icons.menu : Icons.arrow_back_ios,
                  funcaoEsquerda: () {
                    if (widget.veioDeVenda) {
                      Modular.to.pushReplacementNamed("/dashboard/");
                    } else {
                      widget.onMenuPressed.call();
                    }
                  },
                ),
                Text(
                  "Minhas compras",
                  style: Fontes.montserrat.copyWith(
                    fontSize: 20,
                    color: Cores.azul100,
                  ),
                ),
                SizedBox(height: 10),
                TabBar(
                  // Container(color: Colors.black), Container(color: Colors.amber)
                  controller: tabController,
                  labelColor: Cores.vermelho100,
                  indicatorColor: Cores.vermelho100,
                  indicatorWeight: 3,
                  isScrollable: false,
                  labelStyle: Fontes.nunito.copyWith(fontSize: 18, fontWeight: FontWeight.bold),
                  unselectedLabelColor: Colors.black38,
                  onTap: (value) {
                    setState(() {});
                  },
                  tabs: [Tab(text: "Em andamento"), Tab(text: "Historico")],
                ),
                Expanded(
                  child: AnimatedCrossFade(
                    crossFadeState: tabController.index == 0 ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                    duration: Duration(milliseconds: 200),
                    firstChild: EmAndamentoTab(),
                    secondChild: HistoricoTab(itemCount: 5),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
