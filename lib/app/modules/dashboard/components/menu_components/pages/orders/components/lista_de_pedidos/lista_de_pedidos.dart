import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/components/card_pedido/card_pedido.dart';

class ListaDePedidos extends StatelessWidget {
  final int itemCount;

  const ListaDePedidos({Key key, this.itemCount = 1}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView.builder(
        itemCount: itemCount,
        itemBuilder: (context, index) {
          return CardPedido();
        },
      ),
    );
  }
}
