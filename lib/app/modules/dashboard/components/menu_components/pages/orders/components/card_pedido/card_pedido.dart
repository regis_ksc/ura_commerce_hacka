import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/button/button_widget.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/components/card_pedido/botoes_pedido/botoes_pedido.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/components/card_pedido/container_total/container_total.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/components/card_pedido/quantidade_de_produto/quantidade_de_produto.dart';

class CardPedido extends StatelessWidget {
  final String caminhoImg;
  final String nome;
  final String numeroPedido;

  const CardPedido({Key key, this.caminhoImg, this.nome, this.numeroPedido}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Container(
      height: height * 0.3,
      decoration: BoxDecoration(
        color: Cores.amarelo100.withOpacity(0.1),
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          width: 1,
          color: Cores.amarelo50,
        ),
      ),
      margin: EdgeInsets.symmetric(
        horizontal: width * 0.02,
        vertical: height * 0.02,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: width * 0.02,
        vertical: height * 0.01,
      ),
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 30,
            child: Container(
              padding: EdgeInsets.only(left: width * 0.05),
              child: LinhaTitulo(
                caminhoImg: caminhoImg,
                nome: nome,
                numeroPedido: numeroPedido,
                width: width,
              ),
            ),
          ),
          Expanded(
            flex: 45,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: Center(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    QuantidadeDeProduto(
                      produto: "Teclado usb com teclas multimídia",
                      quantidade: 3,
                    ),
                    QuantidadeDeProduto(
                      produto: 'Mouse usb sem fio com bateria',
                      quantidade: 2,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 10,
            child: ContainerTotal(),
          ),
          Expanded(
            flex: 15,
            child: BotoesPedido(
              height: height,
              width: width,
            ),
          ),
        ],
      ),
    );
  }
}

class LinhaTitulo extends StatelessWidget {
  const LinhaTitulo({
    Key key,
    @required this.caminhoImg,
    @required this.nome,
    @required this.numeroPedido,
    @required this.width,
  }) : super(key: key);

  final String caminhoImg;
  final String nome;
  final String numeroPedido;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 15,
          child: Container(
            height: double.infinity,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: CircleAvatar(
                backgroundColor: Cores.azul40,
                // maxRadius: 40,
                backgroundImage: AssetImage(caminhoImg ?? "assets/icons/lojas/tecno.png"),
                radius: 60,
              ),
            ),
          ),
        ),
        Spacer(flex: 10),
        Expanded(
          flex: 60,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Spacer(),
                Text(
                  nome ?? "Info+ Comércio de Eletrônicos",
                  style: Fontes.montserrat.copyWith(fontWeight: FontWeight.bold, fontSize: 14),
                ),
                Spacer(),
                RichText(
                  text: TextSpan(
                      text: numeroPedido ?? "Pedido n°",
                      style: Fontes.montserrat.copyWith(
                        fontWeight: FontWeight.normal,
                        color: Cores.azul100,
                        fontSize: 14,
                      ),
                      children: [
                        TextSpan(
                          text: " 00003",
                          style: Fontes.montserrat.copyWith(
                            color: Cores.azul100,
                            fontWeight: FontWeight.w900,
                            fontSize: 14,
                          ),
                        )
                      ]),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 15,
          child: Column(
            children: <Widget>[
              Spacer(),
              Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(right: width * 0.01),
                child: Image.asset('assets/icons/rating.png'),
              ),
              Spacer(
                flex: 3,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
