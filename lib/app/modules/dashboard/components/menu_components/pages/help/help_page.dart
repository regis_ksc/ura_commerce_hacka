import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'help_controller.dart';

class HelpPage extends KFDrawerContent {
  final String title;
  HelpPage({Key key, this.title = "Help"});

  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends ModularState<HelpPage, HelpController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
