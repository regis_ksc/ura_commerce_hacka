import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa_controller.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_expandido/card_expandido.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/view_pesquisa/conteudo_loja/conteudo_loja.dart';

class ViewPesquisa extends StatelessWidget {
  final BarraDePesquisaController barraDePesquisaController;

  const ViewPesquisa({Key key, this.barraDePesquisaController}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        CardExpandido(
          caminho: "assets/icons/lojas/mercadin.png",
          conteudo: ConteudoLoja(
            categoria: "Mercado",
            titulo: "Mercado Verde",
          ),
        ),
        CardExpandido(
          caminho: "assets/icons/lojas/hero.png",
          conteudo: ConteudoLoja(
            categoria: "Infatil",
            titulo: "Hero Universe",
          ),
        ),
        CardExpandido(
          caminho: "assets/icons/lojas/compra_facil.png",
          conteudo: ConteudoLoja(
            categoria: "Mercado",
            titulo: "Compra Facil",
          ),
        ),
        CardExpandido(
          caminho: "assets/icons/lojas/tecno.png",
          conteudo: ConteudoLoja(
            categoria: "Tecnologia",
            titulo: "Info +",
          ),
        ),
        CardExpandido(),
        CardExpandido(),
        CardExpandido(),
        CardExpandido(),
      ],
    );
  }
}
