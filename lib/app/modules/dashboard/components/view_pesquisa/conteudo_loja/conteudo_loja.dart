import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';

import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class ConteudoLoja extends StatelessWidget {
  final String titulo;
  final String categoria;

  const ConteudoLoja({Key key, this.titulo, this.categoria}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Center(child: Titulo(titulo: titulo)),
          Spacer(flex: 2),
          LinhaCategoria(categoria: categoria),
          Spacer(flex: 2),
          LinhaHorario(),
          Spacer(),
        ],
      ),
    );
  }
}

class Titulo extends StatelessWidget {
  final String titulo;
  const Titulo({
    Key key,
    this.titulo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      titulo ?? "",
      style: Fontes.nunito.copyWith(
        fontSize: 18,
        fontWeight: FontWeight.bold,
        color: Cores.azul100,
      ),
    );
  }
}

class LinhaHorario extends StatelessWidget {
  const LinhaHorario({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(flex: 15, child: Image.asset("assets/icons/relogio.png")),
        Spacer(flex: 5),
        Expanded(
          flex: 80,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Segunda a sábado",
                style: Fontes.nunito.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "07:00 - 18:00",
                style: Fontes.nunito.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class LinhaCategoria extends StatelessWidget {
  final String categoria;
  const LinhaCategoria({
    Key key,
    this.categoria,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(flex: 15, child: Image.asset("assets/icons/etiqueta.png")),
        Spacer(
          flex: 5,
        ),
        Expanded(
          flex: 80,
          child: Text(
            categoria ?? "Loja",
            style: Fontes.nunito.copyWith(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
