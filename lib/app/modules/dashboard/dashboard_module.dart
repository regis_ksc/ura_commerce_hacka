import 'package:ura_commerce_hacka/app/modules/dashboard/pages/categories/categories_page.dart';

import 'components/menu_components/pages/cupons/cupons_controller.dart';
import 'components/menu_components/pages/help/help_controller.dart';
import 'components/menu_components/pages/profile/profile_controller.dart';
import 'pages/categories/categories_controller.dart';
import 'dashboard_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import 'dashboard_page.dart';
import 'drawer_page.dart';
import 'modules/cart/cart_module.dart';
import 'modules/shop/shop_module.dart';

class DashboardModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => CategoriesController()),
        Bind((i) => CuponsController()),
        Bind((i) => HelpController()),
        Bind((i) => DashboardController()),
        Bind((i) => ProfileController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => DrawerPage()),
        Router(
          '/cart',
          module: CartModule(),
          transition: TransitionType.rightToLeftWithFade,
        ),
        Router(
          '/shop',
          module: ShopModule(),
          transition: TransitionType.rightToLeftWithFade,
        ),
        Router(
          '/categories',
          child: (context, args) => CategoriesPage(),
          transition: TransitionType.rightToLeftWithFade,
        ),
      ];

  static Inject get to => Inject<DashboardModule>.of();
}
