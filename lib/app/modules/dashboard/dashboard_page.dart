import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/anuncio_footer/anuncio_footer.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa_controller.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/header_interno/header_interno.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/view_pesquisa/view_pesquisa.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/view_resumo/view_resumo.dart';

import 'dashboard_controller.dart';

// ignore: must_be_immutable
class DashboardPage extends KFDrawerContent {
  final String title;
  DashboardPage({Key key, this.title = "Dashboard"});

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends ModularState<DashboardPage, DashboardController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        widget.onMenuPressed.call();
      },
      child: SafeArea(
        child: Center(
          //
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              HeaderInterno(
                iconeEsquerdo: Icons.menu,
                funcaoEsquerda: () {
                  widget.onMenuPressed.call();
                },
                dinossauro: true,
                funcaoDireita: () {
                  Modular.to.pushNamed('/dashboard/cart');
                },
                iconeDireito: Icons.shopping_cart,
              ),

              //

              BarraDePesquisa(
                controller: controller.barraDePesquisaController,
                funcaoDeBusca: (String text) {
                  setState(() {
                    if (controller.barraDePesquisaController.c.text.length > 0) {
                      controller.setIsPesquisa(true);
                    } else {
                      controller.setIsPesquisa(false);
                    }
                  });
                },
              ),

              //

              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                  margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
                  child: Visibility(
                    visible: controller.isPesquisa,
                    child: ViewPesquisa(
                      barraDePesquisaController: controller.barraDePesquisaController,
                    ),
                    replacement: ViewResumo(),
                  ),
                ),
              ),
              Container(alignment: Alignment.bottomCenter, child: AnuncioFooter()),
            ],
          ),
        ),
      ),
    );
  }
}
