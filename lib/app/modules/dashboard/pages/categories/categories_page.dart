import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/anuncio_footer/anuncio_footer.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_expandido/card_expandido.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/header_interno/header_interno.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/view_pesquisa/conteudo_loja/conteudo_loja.dart';
import 'categories_controller.dart';

class CategoriesPage extends StatefulWidget {
  final String title;
  const CategoriesPage({Key key, this.title = "Categories"}) : super(key: key);

  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends ModularState<CategoriesPage, CategoriesController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: LayoutBuilder(
          builder: (context, constraints) {
            final double height = constraints.biggest.height;
            final double width = constraints.biggest.width;
            List<int> a = [1, 2, 3, 4];
            return Column(
              children: <Widget>[
                Container(
                  width: width,
                  child: HeaderInterno(
                    iconeEsquerdo: Icons.arrow_back_ios,
                    corIconeEsquerdo: Cores.amarelo100,
                    iconeDireito: Icons.shopping_cart,
                    funcaoEsquerda: () {
                      Modular.to.pop();
                    },
                  ),
                ),
                Container(
                  width: width,
                  child: BarraDePesquisa(
                    label: "",
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  width: width,
                  child: Text(
                    "Mercado",
                    textAlign: TextAlign.center,
                    style: Fontes.nunito.copyWith(
                      fontSize: 20,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: 5,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return Stack(
                        alignment: Alignment.center,
                        overflow: Overflow.visible,
                        children: [
                          Positioned(
                            child: CardExpandido(
                              // caminho: "assets/logos/dinossauro.png",
                              conteudo: ConteudoLoja(
                                titulo: "Lojinha",
                                categoria: "PetShop",
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                //ANUNCIO
                AnuncioFooter(),
              ],
            );
          },
        ),
      ),
    );
  }
}
