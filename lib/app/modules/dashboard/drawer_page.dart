import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/button/button_widget.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/molecules/custom_alert/custom_alert_widget.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/molecules/unreleased_alert/unreleased_alert.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/menu_header/menu_header.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/onboarding_page.dart';
import 'package:url_launcher/url_launcher.dart';

import 'components/menu_components/pages/orders/orders_page.dart';
import 'components/menu_components/pages/profile/profile_page.dart';
import 'dashboard_page.dart';

class DrawerPage extends StatefulWidget {
  final String title;
  const DrawerPage({Key key, this.title = "Drawer"}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  KFDrawerController drawerController;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    drawerController = KFDrawerController(
      initialPage: DashboardPage(),
      items: [
        KFDrawerItem.initWithPage(
          text: Text(
            "Inicio",
            style: Fontes.nunito.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Icon(
              Icons.home,
              color: Cores.amarelo75,
            ),
          ),
          page: DashboardPage(),
          // onPressed: () {},
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            "Minha Conta",
            style: Fontes.nunito.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/icons/menu/perfil.png'),
          ),
          page: ProfilePage(),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            "Minhas compras",
            style: Fontes.nunito.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/icons/menu/compras.png'),
          ),
          page: OrdersPage(),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            "Minhas vendas",
            style: Fontes.nunito.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/icons/menu/vendas.png'),
          ),
          // page: SalesPage(),
          onPressed: () => showDialog(context: context, builder: (context) => UnreleasedAlert()),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            "Cupons",
            style: Fontes.nunito.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/icons/menu/cupons.png'),
          ),
          onPressed: () => showDialog(context: context, builder: (context) => UnreleasedAlert()),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            "COVID-19",
            style: Fontes.nunito.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/icons/menu/covid.png'),
          ),
          onPressed: () async {
            showDialog(
              context: context,
              builder: (context) {
                return CustomAlertWidget(
                  verticalMarginPercentage: 24,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Spacer(flex: 2),
                        Text(
                          "Minas Gerais",
                          style: Fontes.montserrat.copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Cores.azul100,
                          ),
                        ),
                        ButtonWidget(
                          margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1),
                          borderRadius: 10,
                          child: Text(
                            "Portal",
                            style: Fontes.montserrat.copyWith(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onTap: () async {
                            const url = 'http://coronavirus.saude.mg.gov.br/';
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                        ),
                        Spacer(),
                        ButtonWidget(
                          margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1),
                          borderRadius: 10,
                          child: Text(
                            "Decretos ",
                            style: Fontes.montserrat.copyWith(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onTap: () async {
                            const url = 'http://coronavirus.saude.mg.gov.br/legislacao/decretos';
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                        ),
                        Spacer(flex: 2),
                        Text(
                          "Uberaba",
                          style: Fontes.montserrat.copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Cores.azul100,
                          ),
                        ),
                        ButtonWidget(
                          margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1),
                          borderRadius: 10,
                          child: Text(
                            "Portal",
                            style: Fontes.montserrat.copyWith(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          foregroundColor: Colors.white,
                          onTap: () async {
                            const url = 'http://uberabacontracovid.com.br/portal/conteudo,49164';
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                        ),
                        Spacer(),
                        ButtonWidget(
                          margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1),
                          borderRadius: 10,
                          child: Text(
                            "Decretos ",
                            style: Fontes.montserrat.copyWith(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          foregroundColor: Colors.white,
                          onTap: () async {
                            const url = 'http://www.uberaba.mg.gov.br/portal/conteudo,49175';
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                        ),
                        Spacer(flex: 2),
                      ],
                    ),
                  ),
                );
              },
            );
          },
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            "Ajuda",
            style: Fontes.nunito.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/icons/menu/ajuda.png'),
          ),
          onPressed: () => showDialog(context: context, builder: (context) => UnreleasedAlert()),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: KFDrawer(
        controller: drawerController,
        animationDuration: Duration(milliseconds: 400),
        header: Align(
          alignment: Alignment.bottomLeft,
          child: MenuHeader(),
        ),
        footer: KFDrawerItem(
          text: Text(
            'Sair',
            style: Fontes.nunito.copyWith(
              color: Color(0xFFA2A2A2),
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          icon: Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/icons/menu/logout.png'),
          ),
          onPressed: () {
            Modular.to.pushReplacementNamed('/onboarding');
          },
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Cores.azul100, Cores.azul100.withOpacity(0.95)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
      ),
    );
  }
}
