import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa_controller.dart';

part 'dashboard_controller.g.dart';

class DashboardController = _DashboardControllerBase with _$DashboardController;

abstract class _DashboardControllerBase extends Disposable with Store {
  @observable
  BarraDePesquisaController barraDePesquisaController = BarraDePesquisaController();

  dispose() {}
  @observable
  bool isPesquisa = false;
  @action
  setIsPesquisa(bool value) => isPesquisa = value;
}
