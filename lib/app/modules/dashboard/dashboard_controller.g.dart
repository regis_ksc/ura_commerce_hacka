// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DashboardController on _DashboardControllerBase, Store {
  final _$barraDePesquisaControllerAtom =
      Atom(name: '_DashboardControllerBase.barraDePesquisaController');

  @override
  BarraDePesquisaController get barraDePesquisaController {
    _$barraDePesquisaControllerAtom.reportRead();
    return super.barraDePesquisaController;
  }

  @override
  set barraDePesquisaController(BarraDePesquisaController value) {
    _$barraDePesquisaControllerAtom
        .reportWrite(value, super.barraDePesquisaController, () {
      super.barraDePesquisaController = value;
    });
  }

  final _$isPesquisaAtom = Atom(name: '_DashboardControllerBase.isPesquisa');

  @override
  bool get isPesquisa {
    _$isPesquisaAtom.reportRead();
    return super.isPesquisa;
  }

  @override
  set isPesquisa(bool value) {
    _$isPesquisaAtom.reportWrite(value, super.isPesquisa, () {
      super.isPesquisa = value;
    });
  }

  final _$_DashboardControllerBaseActionController =
      ActionController(name: '_DashboardControllerBase');

  @override
  dynamic setIsPesquisa(bool value) {
    final _$actionInfo = _$_DashboardControllerBaseActionController.startAction(
        name: '_DashboardControllerBase.setIsPesquisa');
    try {
      return super.setIsPesquisa(value);
    } finally {
      _$_DashboardControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
barraDePesquisaController: ${barraDePesquisaController},
isPesquisa: ${isPesquisa}
    ''';
  }
}
