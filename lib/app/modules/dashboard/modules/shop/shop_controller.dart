import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'shop_controller.g.dart';

class ShopController = _ShopControllerBase with _$ShopController;

abstract class _ShopControllerBase extends Disposable with Store {
  dispose() {}
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
