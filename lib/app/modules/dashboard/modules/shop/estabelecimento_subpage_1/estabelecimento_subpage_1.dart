import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/pages/product/components/produto_detalhes/produto_detalhes.dart';

class EstabelecimentoSubpage1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return Column(children: [
      SizedBox(height: altura * 0.02),
      BarraDePesquisa(label: 'Pesquisar produtos da loja'),
      SizedBox(height: altura * 0.02),
      Text(
        'Produtos Disponíveis',
        style: Fontes.nunito.copyWith(
          decoration: TextDecoration.none,
          fontSize: 20,
          color: Cores.azul100,
          letterSpacing: 0.5,
          fontWeight: FontWeight.w600,
        ),
      ),
      SizedBox(height: altura * 0.02),
      Expanded(
        child: ListView(
          children: <Widget>[
            ProdutoDetalhes(
              caminhoImagem: 'assets/teclado.png',
              nome: 'TECLADO USB',
              descricao: 'Teclado usb com teclas multimídeas',
              preco: 'RS 39,90',
              precoDividido: '3x de RS13,30',
            ),
            ProdutoDetalhes(
              caminhoImagem: 'assets/teclado.png',
              nome: 'Headset Gaming',
              descricao: 'Headset com mic. e iluminação',
              preco: 'RS 67,90',
              precoDividido: '3x de RS21,30',
            ),
            ProdutoDetalhes(
              caminhoImagem: 'assets/teclado.png',
              nome: 'Monitor LED',
              descricao: 'Monitor Full Hd 60Hz',
              preco: 'RS 67,90',
              precoDividido: '3x de RS21,30',
            )
          ],
        ),
      ),
    ]);
  }
}
