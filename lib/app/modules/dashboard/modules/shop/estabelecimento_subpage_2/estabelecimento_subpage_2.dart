import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class EstabelecimentoSubpage2 extends StatelessWidget {
  final String categoria;
  final String endereco;
  final String telefone;
  final String funcionamento;

  const EstabelecimentoSubpage2({Key key, this.categoria, this.endereco, this.telefone, this.funcionamento})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Container(
          height: altura * 0.15,
          width: largura,
          color: Cores.azul100.withOpacity(0.08),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  categoria,
                  textAlign: TextAlign.left,
                  style: Fontes.nunito.copyWith(
                    decoration: TextDecoration.none,
                    fontSize: 15,
                    color: Colors.black,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  endereco,
                  textAlign: TextAlign.left,
                  style: Fontes.nunito.copyWith(
                    decoration: TextDecoration.none,
                    fontSize: 15,
                    color: Colors.black,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  telefone,
                  textAlign: TextAlign.left,
                  style: Fontes.nunito.copyWith(
                    decoration: TextDecoration.none,
                    fontSize: 15,
                    color: Colors.black,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  funcionamento,
                  textAlign: TextAlign.left,
                  style: Fontes.nunito.copyWith(
                    decoration: TextDecoration.none,
                    fontSize: 15,
                    color: Colors.black,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          child: Image.asset('assets/maps.png'),
          width: largura,
          height: altura * 0.35,
        )
      ],
    );
  }
}
