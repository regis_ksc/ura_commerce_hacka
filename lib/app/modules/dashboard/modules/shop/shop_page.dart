import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/header_interno/header_interno.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/estabelecimento_subpage_2/estabelecimento_subpage_2.dart';
import 'estabelecimento_subpage_1/estabelecimento_subpage_1.dart';
import 'shop_controller.dart';

class ShopPage extends StatefulWidget {
  final String title;
  const ShopPage({Key key, this.title = "Shop"}) : super(key: key);

  @override
  _ShopPageState createState() => _ShopPageState();
}

class _ShopPageState extends ModularState<ShopPage, ShopController> with TickerProviderStateMixin {
  //use 'controller' variable to access controller

  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      vsync: this,
      initialIndex: 0,
      length: 2,
    );
  }

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Material(
        child: DefaultTabController(
          length: 2,
          initialIndex: 0,
          child: Scaffold(
            body: Container(
              color: Colors.white,
              child: Column(
                children: [
                  HeaderInterno(
                    caminhoImg: "assets/icons/lojas/tecno.png",
                    iconeEsquerdo: Icons.arrow_back_ios,
                    funcaoEsquerda: () {
                      Modular.to.pop();
                    },
                  ),
                  SizedBox(height: altura * 0.02),
                  Text(
                    'Info+ Comercio de Eletrônicos',
                    style: Fontes.nunito.copyWith(
                      decoration: TextDecoration.none,
                      fontSize: 20,
                      color: Cores.azul100,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TabBar(
                    controller: tabController,
                    labelColor: Cores.vermelho100,
                    indicatorColor: Cores.vermelho100,
                    indicatorWeight: 3,
                    isScrollable: false,
                    labelStyle: Fontes.nunito.copyWith(fontSize: 18, fontWeight: FontWeight.bold),
                    unselectedLabelColor: Colors.black38,
                    onTap: (value) {
                      setState(() {});
                    },
                    tabs: [Tab(text: "Produtos"), Tab(text: "Sobre")],
                  ),
                  Expanded(
                    child: Visibility(
                      visible: tabController.index == 0,
                      child: EstabelecimentoSubpage1(),
                      replacement: EstabelecimentoSubpage2(
                        categoria: 'Eletrônicos',
                        endereco: 'Av. Doutor Fidélis Réis, 288, Centro - Uberaba',
                        telefone: 'Tel:(34) 3338-9534 ',
                        funcionamento: 'Segunda a sábado 07:00 - 18:00',
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
