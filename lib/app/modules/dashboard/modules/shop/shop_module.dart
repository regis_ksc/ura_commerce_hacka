import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/pages/product/product_controller.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/pages/product/product_page.dart';

import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/shop_controller.dart';

import 'package:flutter_modular/flutter_modular.dart';
import 'shop_page.dart';

class ShopModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => ProductController()),
        Bind((i) => ShopController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => ShopPage()),
        Router('/produto', child: (_, args) => ProductPage()),
      ];

  static Inject get to => Inject<ShopModule>.of();
}
