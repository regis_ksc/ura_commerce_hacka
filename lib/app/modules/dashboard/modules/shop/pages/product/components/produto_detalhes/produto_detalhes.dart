import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_expandido/card_expandido.dart';

class ProdutoDetalhes extends StatelessWidget {
  final String nome;
  final String descricao;
  final String preco;
  final String precoDividido;
  final String caminhoImagem;

  const ProdutoDetalhes({Key key, this.nome, this.descricao, this.preco, this.precoDividido, this.caminhoImagem})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Modular.to.pushNamed('dashboard/shop/produto');
        },
        child: CardExpandido(
          flexImg: 20,
          caminho: caminhoImagem,
          conteudo: Row(
            children: [
              Expanded(
                flex: 65,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      this.nome,
                      style: Fontes.nunito.copyWith(
                        decoration: TextDecoration.none,
                        fontSize: 15,
                        color: Cores.azul100,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: altura * 0.01),
                      child: Text(
                        this.descricao,
                        textAlign: TextAlign.center,
                        style: Fontes.nunito.copyWith(
                          decoration: TextDecoration.none,
                          fontSize: 14,
                          color: Cores.azul70,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 35,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      this.preco,
                      textAlign: TextAlign.center,
                      style: Fontes.nunito.copyWith(
                        decoration: TextDecoration.none,
                        fontSize: 14,
                        color: Cores.vermelho100,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      this.precoDividido,
                      textAlign: TextAlign.center,
                      style: Fontes.nunito.copyWith(
                        decoration: TextDecoration.none,
                        fontSize: 14,
                        color: Cores.vermelho100,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
