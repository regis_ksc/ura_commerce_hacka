import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/header_interno/header_interno.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/cart/components/botao_rodape/botao_rodape.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/pages/product/components/caracteristica_produto/caracteristica_produto.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/pages/product/components/produto_detalhes/produto_detalhes.dart';
import 'product_controller.dart';

class ProductPage extends StatefulWidget {
  final String title;
  const ProductPage({Key key, this.title = "Product"}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends ModularState<ProductPage, ProductController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              HeaderInterno(
                iconeEsquerdo: Icons.arrow_back_ios,
                funcaoEsquerda: () {},
              ),
              Text(
                'DETALHE DO PRODUTO',
                style: Fontes.nunito.copyWith(
                  decoration: TextDecoration.none,
                  fontSize: 20,
                  color: Cores.vermelho100,
                  letterSpacing: 0.5,
                  fontWeight: FontWeight.w600,
                ),
              ),
              ProdutoDetalhes(
                caminhoImagem: 'assets/teclado.png',
                nome: 'TECLADO USB',
                descricao: 'Teclado usb com teclas multimídeas',
                preco: 'RS 39,90',
                precoDividido: '3x de RS13,30',
              ),
              CaracteristicaProduto(caracteristica: 'Conectividade:', escolhido: 'Com fio'),
              CaracteristicaProduto(
                caracteristica: 'Cor:',
                escolhido: 'Preto',
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.only(bottom: largura * 0.1),
                child: Container(
                  child: BotaoRodape(
                    texto: 'ADICIONAR AO CARRINHO',
                    corTexto: Cores.azul100,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
