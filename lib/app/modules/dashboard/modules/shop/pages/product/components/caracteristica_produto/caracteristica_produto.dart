import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class CaracteristicaProduto extends StatelessWidget {
  final String caracteristica;
  final String escolhido;

  const CaracteristicaProduto({Key key, this.caracteristica, this.escolhido}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Container(
            width: largura,
            height: altura * 0.13,
            color: Cores.amarelo50,
            child: Row(
              children: [
                Expanded(
                    flex: 70,
                    child: Padding(
                      padding: EdgeInsets.only(left: largura * 0.03),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            caracteristica,
                            style: Fontes.nunito.copyWith(
                              decoration: TextDecoration.none,
                              fontSize: 20,
                              color: Cores.azul70,
                              letterSpacing: 0.5,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            height: altura * 0.01,
                          ),
                          Text(
                            '0 de 1',
                            style: Fontes.nunito.copyWith(
                              decoration: TextDecoration.none,
                              fontSize: 15,
                              color: Colors.grey,
                              letterSpacing: 0.5,
                              fontWeight: FontWeight.w600,
                            ),
                          )
                        ],
                      ),
                    )),
                Expanded(
                    flex: 30,
                    child: Padding(
                      padding: EdgeInsets.only(right: largura * 0.02),
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        height: altura * 0.05,
                        width: largura * 0.18,
                        child: Text(
                          'OBRIGATÓRIO',
                          style: Fontes.nunito.copyWith(
                            decoration: TextDecoration.none,
                            fontSize: 12,
                            color: Colors.white,
                            letterSpacing: 0.5,
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )),
              ],
            )),
        Container(
            width: largura,
            height: altura * 0.1,
            color: Cores.azul100.withOpacity(0.08),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Cores.azul100.withOpacity(0.15),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  height: altura * 0.055,
                  width: largura * 0.6,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 85,
                        child: Text(
                          escolhido,
                          style: Fontes.nunito.copyWith(
                            decoration: TextDecoration.none,
                            fontSize: 15,
                            color: Colors.black,
                            letterSpacing: 0.5,
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 15,
                        child: Icon(
                          Icons.keyboard_arrow_down,
                          size: 28,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ))
      ],
    );
  }
}
