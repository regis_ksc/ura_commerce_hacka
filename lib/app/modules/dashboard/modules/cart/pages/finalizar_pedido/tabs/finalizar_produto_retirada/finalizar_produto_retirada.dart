import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class FinalizarProdutoRetirada extends StatefulWidget {
  @override
  _FinalizarProdutoRetiradaState createState() => _FinalizarProdutoRetiradaState();
}

class _FinalizarProdutoRetiradaState extends State<FinalizarProdutoRetirada> {
  String nomeCidade = "Cartão de Crédito";

  var _cidades = ['Cartão de Crédito', 'Cartão de Débito', 'Dinheiro'];

  var _itemSelecionado = 'Cartão de Crédito';

  var _cidades2 = ['Minha Casa', 'Meu Apartamento'];

  var _itemSelecionado2 = 'Minha Casa';

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return Expanded(
      flex: 25,
      child: Container(
        width: largura,
        decoration: BoxDecoration(
          color: Cores.azul100.withOpacity(0.15),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
                margin: EdgeInsets.symmetric(horizontal: (largura * 0.05)),
                alignment: Alignment.topCenter,
                child: Text(
                  "Forma de Pagamento",
                  style: Fontes.nunito
                      .copyWith(fontSize: 18, letterSpacing: 0.5, fontWeight: FontWeight.bold, color: Cores.vermelho75),
                )),
            Container(
              alignment: Alignment.center,
              child: criaDropD(),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: (largura * 0.05)),
              alignment: Alignment.bottomCenter,
              child: Text(
                "Endereço de Retirada",
                style: Fontes.nunito
                    .copyWith(fontSize: 18, letterSpacing: 0.5, fontWeight: FontWeight.bold, color: Cores.vermelho75),
              ),
            ),
          ],
        ),
      ),
    );
  }

  criaDropD() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Column(
        children: <Widget>[
          /*TextField(
            enabled: false,
            
            onSubmitted: (String userInput){
              setState(() {                
                debugPrint('chamei setState');
                nomeCidade = userInput;
              });
            },
          ),*/

          DropdownButton<String>(
              items: _cidades.map((String dropDownStringItem) {
                return DropdownMenuItem<String>(
                  value: dropDownStringItem,
                  child: Text(
                    dropDownStringItem,
                    style: TextStyle(backgroundColor: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (String novoItemSelecionado) {
                _dropDownItemSelected(novoItemSelecionado);
                setState(() {
                  this._itemSelecionado = novoItemSelecionado;
                });
              },
              value: _itemSelecionado),
        ],
      ),
    );
  }

  criaDropD2() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Column(
        children: <Widget>[
          /*TextField(
            enabled: false,
            
            onSubmitted: (String userInput){
              setState(() {                
                debugPrint('chamei setState');
                nomeCidade = userInput;
              });
            },
          ),*/

          DropdownButton<String>(
              items: _cidades2.map((String dropDownStringItem) {
                return DropdownMenuItem<String>(
                  value: dropDownStringItem,
                  child: Text(
                    dropDownStringItem,
                    style: TextStyle(backgroundColor: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (String novoItemSelecionado) {
                _dropDownItemSelected2(novoItemSelecionado);
                setState(() {
                  this._itemSelecionado2 = novoItemSelecionado;
                });
              },
              value: _itemSelecionado2),
        ],
      ),
    );
  }

  void _dropDownItemSelected(String novoItem) {
    setState(() {
      this._itemSelecionado = novoItem;
    });
  }

  void _dropDownItemSelected2(String novoItem) {
    setState(() {
      this._itemSelecionado2 = novoItem;
    });
  }
}
