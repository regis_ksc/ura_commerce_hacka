import 'package:mobx/mobx.dart';

part 'finalizar_pedido_controller.g.dart';

class FinalizarPedidoController = _FinalizarPedidoControllerBase
    with _$FinalizarPedidoController;

abstract class _FinalizarPedidoControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
