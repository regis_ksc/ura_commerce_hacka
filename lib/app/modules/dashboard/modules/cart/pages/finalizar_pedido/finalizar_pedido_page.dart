import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:md2_tab_indicator/md2_tab_indicator.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/header_interno/header_interno.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/cart/pages/finalizar_pedido/tabs/finalizar_produto_entrega/finalizar_produto_entrega.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/cart/pages/finalizar_pedido/tabs/finalizar_produto_retirada/finalizar_produto_retirada.dart';
import 'finalizar_pedido_controller.dart';

class FinalizarPedidoPage extends StatefulWidget {
  final String title;
  const FinalizarPedidoPage({Key key, this.title = "FinalizarPedido"}) : super(key: key);

  @override
  _FinalizarPedidoPageState createState() => _FinalizarPedidoPageState();
}

class _FinalizarPedidoPageState extends ModularState<FinalizarPedidoPage, FinalizarPedidoController>
    with TickerProviderStateMixin {
  //use 'controller' variable to access controller
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      vsync: this,
      initialIndex: 0,
      length: 2,
    );
  }

  String nomeCidade = "Cartão de Crédito";
  var _cidades = ['Cartão de Crédito', 'Cartão de Débito', 'Dinheiro'];
  var _itemSelecionado = 'Cartão de Crédito';
  var _cidades2 = ['Minha Casa', 'Meu Apartamento'];
  var _itemSelecionado2 = 'Minha Casa';

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              HeaderInterno(
                iconeEsquerdo: Icons.arrow_back_ios,
                funcaoEsquerda: () {
                  Modular.to.pop();
                },
              ),
              DefaultTabController(
                length: 2,
                child: TabBar(
                  controller: tabController,
                  labelColor: Cores.vermelho100,
                  indicatorColor: Cores.vermelho100,
                  indicatorWeight: 3,
                  isScrollable: false,
                  labelStyle: Fontes.nunito.copyWith(fontSize: 18, fontWeight: FontWeight.bold),
                  unselectedLabelColor: Colors.black38,
                  onTap: (value) {
                    setState(() {});
                  },
                  tabs: [Tab(text: "Entrega"), Tab(text: "Retirada")],
                ),
              ),
              //SUPERIOR
              // FinalizarProdutoEntrada(),
              Visibility(
                visible: tabController.index == 0,
                child: FinalizarProdutoEntrega(),
                replacement: FinalizarProdutoRetirada(),
              ),
              //MEIO
              Expanded(
                flex: 50,
                child: Container(
                  width: largura,
                  decoration: BoxDecoration(
                    color: Cores.azul100.withOpacity(0.15),
                  ),
                  child: Image.asset('assets/maps.png'),
                ),
              ),
              //RODAPÉ
              Expanded(
                flex: 30,
                child: Container(
                  width: largura,
                  height: altura * 0.22,
                  color: Cores.azul100,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: largura * 0.1,
                      right: largura * 0.1,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(''),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 5,
                              child: Text(
                                'Produtos:',
                                textAlign: TextAlign.left,
                                style: Fontes.nunito.copyWith(
                                  decoration: TextDecoration.none,
                                  fontSize: 16,
                                  color: Colors.white,
                                  letterSpacing: 0.5,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Text(
                                'RS 39,90',
                                textAlign: TextAlign.center,
                                style: Fontes.nunito.copyWith(
                                  decoration: TextDecoration.none,
                                  fontSize: 16,
                                  color: Colors.white,
                                  letterSpacing: 0.5,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 5,
                              child: Text(
                                'Entrega:',
                                textAlign: TextAlign.left,
                                style: Fontes.nunito.copyWith(
                                  decoration: TextDecoration.none,
                                  fontSize: 16,
                                  color: Colors.white,
                                  letterSpacing: 0.5,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Text(
                                'RS 12,30',
                                textAlign: TextAlign.center,
                                style: Fontes.nunito.copyWith(
                                  decoration: TextDecoration.none,
                                  fontSize: 16,
                                  color: Colors.white,
                                  letterSpacing: 0.5,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 5,
                              child: Text(
                                'Total:',
                                textAlign: TextAlign.left,
                                style: Fontes.nunito.copyWith(
                                  decoration: TextDecoration.none,
                                  fontSize: 16,
                                  color: Colors.white,
                                  letterSpacing: 0.5,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Text(
                                'RS 52,20',
                                textAlign: TextAlign.center,
                                style: Fontes.nunito.copyWith(
                                  decoration: TextDecoration.none,
                                  fontSize: 16,
                                  color: Colors.white,
                                  letterSpacing: 0.5,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Text(''),
                        GestureDetector(
                          onTap: () {
                            Modular.to.pushNamed('/dashboard/cart/pedidos');
                          },
                          child: Container(
                            width: largura * 0.55,
                            decoration: BoxDecoration(
                              color: Cores.amarelo100,
                              borderRadius: BorderRadius.circular(7.0),
                            ),
                            child: Text(
                              'FAZER O PEDIDO',
                              textAlign: TextAlign.center,
                              style: Fontes.nunito.copyWith(
                                decoration: TextDecoration.none,
                                fontSize: 20,
                                color: Cores.azul100,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
