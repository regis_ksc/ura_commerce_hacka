import 'package:flutter/material.dart';

class CarrinhoQuant extends StatelessWidget {
  final Color cor;
  final double opacidade;
  final double raio;
  final IconData icon;

  const CarrinhoQuant({Key key, this.opacidade, this.raio, this.icon, this.cor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 25,
      height: 25,
      decoration: BoxDecoration(
        color: cor.withOpacity(opacidade),
        border: Border.all(color: cor.withOpacity(opacidade + 1)),
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(raio), topLeft: Radius.circular(raio)),
      ),
      child: Icon(icon,
          color: Colors
              .white), /*icon != null
            ? Icon(icon, color: Colors.white)
            : Text('1',
                textAlign: TextAlign.center,
                style: Fontes.nunito
                    .copyWith(fontSize: 18, fontWeight: FontWeight.bold))*/
    );
  }
}
