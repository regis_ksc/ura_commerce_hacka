import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_retangular/card_retangular.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/cart/components/carrinho_quant/carrinho_quant.dart';

class CarrinhoWidget extends StatelessWidget {
  final String caminho;
  final String produto;
  final String preco;

  const CarrinhoWidget({Key key, this.caminho, this.produto, this.preco}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;

    return Container(
      width: largura,
      height: altura * 0.30,
      decoration: BoxDecoration(
        color: Cores.azul100.withOpacity(0.1),
        border: Border.all(color: Cores.azul100.withOpacity(0.2)),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 30,
                child: CardRetangular(caminho: caminho),
              ),
              Expanded(
                flex: 85,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 75,
                          child: Text(
                            produto,
                            style: Fontes.nunito.copyWith(
                              decoration: TextDecoration.none,
                              fontSize: 16,
                              color: Cores.azul100,
                              letterSpacing: 0.5,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: IconButton(
                              icon: Icon(
                                Icons.remove,
                                color: Cores.vermelho100,
                              ),
                              onPressed: () {}),
                        ),
                      ],
                    ),
                    //Text(''),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 65,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Quantidade:',
                                style: Fontes.nunito.copyWith(
                                  color: Cores.vermelho100,
                                  fontSize: 16,
                                  letterSpacing: 0.5,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  CarrinhoQuant(
                                    cor: Colors.blue,
                                    opacidade: 0.4,
                                    raio: 7.0,
                                    icon: Icons.remove,
                                  ),
                                  CarrinhoQuant(cor: Colors.white),
                                  CarrinhoQuant(
                                    cor: Colors.blue,
                                    opacidade: 0.9,
                                    raio: 7.0,
                                    icon: Icons.add,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 35,
                          child: Text(
                            preco,
                            style: Fontes.nunito.copyWith(
                              decoration: TextDecoration.none,
                              fontSize: 16,
                              color: Cores.vermelho100,
                              letterSpacing: 0.5,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
