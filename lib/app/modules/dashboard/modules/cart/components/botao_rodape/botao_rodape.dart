import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';

class BotaoRodape extends StatelessWidget {
  @required
  final String texto;
  final Color corTexto;
  final Color corFundo;

  const BotaoRodape({Key key, this.texto, this.corTexto, this.corFundo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return InkWell(
      onTap: () {
        Modular.to.pushNamed('dashboard/cart/finalizar');
      },
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: this.corFundo ?? Cores.amarelo100,
          borderRadius: BorderRadius.circular(10),
        ),
        height: altura * 0.063,
        width: largura * 0.75,
        child: Text(
          this.texto,
          style: Fontes.nunito.copyWith(
            decoration: TextDecoration.none,
            fontSize: 16,
            color: this.corTexto ?? Cores.azul100,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
