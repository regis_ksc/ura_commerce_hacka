import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/card_retangular/card_retangular.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/header_interno/header_interno.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/cart/components/botao_rodape/botao_rodape.dart';
import 'cart_controller.dart';

class CartPage extends StatefulWidget {
  final String title;
  const CartPage({Key key, this.title = "Cart"}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends ModularState<CartPage, CartController> {
  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              HeaderInterno(
                iconeEsquerdo: Icons.arrow_back_ios,
                funcaoEsquerda: () {
                  Modular.to.pop();
                },
              ),
              Text(''),
              Text(
                'Carrinho',
                style: Fontes.nunito.copyWith(
                  decoration: TextDecoration.none,
                  fontSize: 25,
                  color: Cores.azul100,
                  letterSpacing: 0.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(''),
              Container(
                width: largura,
                height: altura * 0.19,
                decoration: BoxDecoration(
                  color: Cores.azul100.withOpacity(0.1),
                  border: Border.all(color: Cores.azul100.withOpacity(0.2)),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 30,
                          child: CardRetangular(caminho: 'assets/logos/dinossauro.png'),
                        ),
                        Expanded(
                          flex: 85,
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 75,
                                    child: Text(
                                      'Teclado usb com teclas multimídea',
                                      style: Fontes.nunito.copyWith(
                                        decoration: TextDecoration.none,
                                        fontSize: 16,
                                        color: Cores.azul100,
                                        letterSpacing: 0.5,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 10,
                                    child: Icon(Icons.delete, color: Cores.vermelho100),
                                  ),
                                ],
                              ),
                              Text(' '),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 65,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          'Quantidade:',
                                          style: Fontes.nunito.copyWith(
                                            color: Cores.vermelho100,
                                            fontSize: 16,
                                            letterSpacing: 0.5,
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              width: 25,
                                              height: 25,
                                              decoration: BoxDecoration(
                                                color: Cores.azul100.withOpacity(0.4),
                                                border: Border.all(color: Cores.azul100.withOpacity(0.5)),
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft: Radius.circular(7.0), topLeft: Radius.circular(7.0)),
                                              ),
                                              child: Icon(Icons.remove, color: Colors.white),
                                            ),
                                            Container(
                                              width: 25,
                                              height: 25,
                                              color: Colors.white,
                                              child: Text(
                                                '1',
                                                textAlign: TextAlign.center,
                                                style:
                                                    Fontes.nunito.copyWith(fontSize: 18, fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                            Container(
                                              width: 25,
                                              height: 25,
                                              decoration: BoxDecoration(
                                                color: Cores.azul100.withOpacity(0.9),
                                                border: Border.all(color: Cores.azul100.withOpacity(1)),
                                                borderRadius: BorderRadius.only(
                                                    bottomRight: Radius.circular(7.0), topRight: Radius.circular(7.0)),
                                              ),
                                              child: Icon(Icons.add, color: Colors.white),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 35,
                                    child: Text(
                                      'RS 39,90 Até 3x sem Juros',
                                      style: Fontes.nunito.copyWith(
                                        decoration: TextDecoration.none,
                                        fontSize: 16,
                                        color: Cores.vermelho100,
                                        letterSpacing: 0.5,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              /* CarrinhoWidget(
                  caminho: 'assets/teclado.png',
                  produto: 'Teclado usb com teclas multimídea',
                  preco: 'RS 39,90 Até 3x sem Juros'),*/
              Spacer(),
              Container(
                width: largura,
                height: altura * 0.22,
                color: Cores.azul100,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: largura * 0.1,
                    right: largura * 0.1,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(''),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 5,
                            child: Text(
                              'Produtos:',
                              textAlign: TextAlign.left,
                              style: Fontes.nunito.copyWith(
                                decoration: TextDecoration.none,
                                fontSize: 16,
                                color: Colors.white,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Text(
                              'RS 39,90',
                              textAlign: TextAlign.center,
                              style: Fontes.nunito.copyWith(
                                decoration: TextDecoration.none,
                                fontSize: 16,
                                color: Colors.white,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 5,
                            child: Text(
                              'Entrega:',
                              textAlign: TextAlign.left,
                              style: Fontes.nunito.copyWith(
                                decoration: TextDecoration.none,
                                fontSize: 16,
                                color: Colors.white,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Text(
                              'RS 12,30',
                              textAlign: TextAlign.center,
                              style: Fontes.nunito.copyWith(
                                decoration: TextDecoration.none,
                                fontSize: 16,
                                color: Colors.white,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 5,
                            child: Text(
                              'Total:',
                              textAlign: TextAlign.left,
                              style: Fontes.nunito.copyWith(
                                decoration: TextDecoration.none,
                                fontSize: 16,
                                color: Colors.white,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Text(
                              'RS 52,20',
                              textAlign: TextAlign.center,
                              style: Fontes.nunito.copyWith(
                                decoration: TextDecoration.none,
                                fontSize: 16,
                                color: Colors.white,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Text(''),
                      BotaoRodape(
                        texto: 'FAZER O PEDIDO',
                        corTexto: Cores.azul100,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
