import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/orders/orders_page.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/cart/pages/finalizar_pedido/finalizar_pedido_controller.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/cart/pages/finalizar_pedido/finalizar_pedido_page.dart';

import 'cart_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import 'cart_page.dart';

class CartModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => FinalizarPedidoController()),
        Bind((i) => CartController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => CartPage()),
        Router('/finalizar', child: (_, args) => FinalizarPedidoPage()),
        Router('/pedidos', child: (_, args) => OrdersPage(veioDeVenda: true)),
      ];

  static Inject get to => Inject<CartModule>.of();
}
