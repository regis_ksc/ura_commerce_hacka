import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_controller.dart';

class AppWidget extends StatelessWidget {
  final appController = Modular.get<AppController>();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        appController.setCurrentFocus(FocusScope.of(context));
      },
      onPanUpdate: (details) {
        appController.setCurrentFocus(FocusScope.of(context));
      },
      child: MaterialApp(
        navigatorKey: Modular.navigatorKey,
        title: 'Flutter Slidy',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            // primarySwatch: Colors.blue,
            ),
        initialRoute: '/',
        onGenerateRoute: Modular.generateRoute,
      ),
    );
  }
}
