import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/button/button_widget.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/molecules/custom_alert/custom_alert_widget.dart';

class UnreleasedAlert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomAlertWidget(
      onTapInside: null,
      verticalMarginPercentage: 20,
      child: FractionallySizedBox(
        widthFactor: 0.85,
        heightFactor: 0.9,
        child: Animator(
          tween: Tween<double>(begin: 0, end: 1),
          builder: (context, animatorState, child) {
            return AnimatedOpacity(
              duration: const Duration(milliseconds: 200),
              opacity: animatorState.animation.value,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 50,
                    child: SvgPicture.asset(
                      "assets/unreleased.svg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  Expanded(
                    flex: 20,
                    child: Container(
                      child: Text(
                        "Humm... esse recurso ainda não está disponível",
                        textAlign: TextAlign.center,
                        style: Fontes.montserrat.copyWith(
                          color: Colors.grey[800],
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 20,
                    child: Container(
                      child: Text(
                        "Mas calma! Nossa equipe está trabalhando para te entregar em breve",
                        textAlign: TextAlign.center,
                        style: Fontes.montserrat.copyWith(
                          fontSize: 16,
                          color: Colors.grey[600],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    child: FractionallySizedBox(
                      widthFactor: 0.75,
                      child: ButtonWidget(
                        onTap: () => Modular.to.pop(),
                        borderRadius: 10,
                        child: Center(
                          child: Text(
                            'ENTENDI',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              letterSpacing: 0.5,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
          child: Column(),
        ),
      ),
    );
  }
}
