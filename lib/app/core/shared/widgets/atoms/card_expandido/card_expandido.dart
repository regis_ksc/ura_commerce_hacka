import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';

class CardExpandido extends StatelessWidget {
  final double margem;
  final double tamanho;
  final int flexImg;
  final Widget conteudo;
  final String caminho;

  const CardExpandido({
    Key key,
    this.margem = 5,
    this.tamanho = 15,
    this.flexImg = 40,
    this.caminho,
    this.conteudo,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    final double larguraPorCento = largura / 100;
    final double alturaPorCento = altura / 100;
    final double lado = largura / 5;
    return Center(
      child: Container(
        // color: Colors.red,
        width: largura,
        margin: EdgeInsets.symmetric(
          horizontal: larguraPorCento * margem,
        ),
        child: Container(
          width: largura,
          height: alturaPorCento * tamanho,
          margin: EdgeInsets.symmetric(vertical: alturaPorCento * 1),
          decoration: BoxDecoration(
            color: Cores.azul100.withOpacity(0.1),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Cores.azul100.withOpacity(0.2)),
          ),
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: lado / 100 * 5,
              horizontal: lado / 100 * 5,
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: flexImg,
                  child: Container(
                    child: Image.asset(
                      caminho ?? 'assets/logos/dinossauro.png',
                      color: caminho == null ? Cores.azul100 : null,
                    ),
                  ),
                ),
                Expanded(
                  flex: 100 - flexImg,
                  child: Container(child: conteudo),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
