import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/fontes.dart';
import 'package:ura_commerce_hacka/app/core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa_controller.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/dashboard_controller.dart';

import '../../../../../app_controller.dart';

class BarraDePesquisa extends StatefulWidget {
  final Function funcaoDeBusca;
  final BarraDePesquisaController controller;
  final String label;

  const BarraDePesquisa({Key key, this.funcaoDeBusca, this.label = "", this.controller}) : super(key: key);

  @override
  _BarraDePesquisaState createState() => _BarraDePesquisaState(controller ?? BarraDePesquisaController());
}

class _BarraDePesquisaState extends State<BarraDePesquisa> {
  final BarraDePesquisaController controller;
  final appController = Modular.get<AppController>();

  _BarraDePesquisaState(this.controller);
  @override
  Widget build(BuildContext context) {
    final borderProps = OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
      borderSide: BorderSide(color: Cores.azul100.withOpacity(0.25), width: 2.5),
    );

    return Container(
      margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
      decoration: BoxDecoration(color: Cores.azul100.withOpacity(0.1), borderRadius: BorderRadius.circular(30)),
      child: TextField(
        onChanged: widget.funcaoDeBusca,
        controller: controller.c,
        style: Fontes.nunito.copyWith(
          fontSize: 16,
          color: Cores.azul100,
          fontWeight: FontWeight.bold,
          letterSpacing: 1,
        ),
        decoration: InputDecoration(
          hintText: widget.label,
          hintStyle: Fontes.nunito.copyWith(color: Cores.azul50, fontWeight: FontWeight.bold),
          prefixIcon: Icon(
            Icons.search,
            color: Cores.azul50,
          ),
          suffixIcon: Visibility(
            visible: controller.c.text.isNotEmpty,
            child: IconButton(
              icon: Icon(Icons.clear, color: Colors.redAccent[200]),
              onPressed: () {
                setState(() {
                  controller.c.text = "";
                  Modular.get<DashboardController>().setIsPesquisa(false);
                  if (appController.keyboardIsShown) {
                    appController.setCurrentFocus(FocusScope.of(context));
                  }
                });
              },
            ),
          ),
          focusedBorder: borderProps,
          enabledBorder: borderProps,
        ),
      ),
    );
  }
}
