import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'barra_de_pesquisa_controller.g.dart';

class BarraDePesquisaController = _BarraDePesquisaControllerBase with _$BarraDePesquisaController;

abstract class _BarraDePesquisaControllerBase with Store {
  @observable
  TextEditingController c = TextEditingController();
}
