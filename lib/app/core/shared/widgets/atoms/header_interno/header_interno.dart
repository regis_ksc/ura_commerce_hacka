import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';

class HeaderInterno extends StatelessWidget {
  final IconData iconeEsquerdo;
  final IconData iconeDireito;
  final Color corIconeEsquerdo;
  final Function funcaoEsquerda;
  final Function funcaoDireita;
  final String caminhoImg;
  final bool dinossauro;

  const HeaderInterno({
    Key key,
    @required this.iconeEsquerdo,
    this.iconeDireito,
    @required this.funcaoEsquerda,
    this.funcaoDireita,
    this.dinossauro,
    this.corIconeEsquerdo,
    this.caminhoImg,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width;
    final double altura = MediaQuery.of(context).size.height;
    return Container(
      width: largura,
      height: altura * 0.1,
      margin: EdgeInsets.only(top: altura * 0.01),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 15,
            child: Column(
              children: <Widget>[
                Expanded(
                    flex: 65,
                    child: Container(
                      child: Center(
                        child: IconButton(
                          icon: Icon(
                            iconeEsquerdo,
                            color: corIconeEsquerdo ?? Cores.azul75,
                            size: largura * 0.08,
                          ),
                          onPressed: funcaoEsquerda,
                        ),
                      ),
                    )),
                Spacer(flex: 35),
              ],
            ),
          ),
          Expanded(
            flex: 70,
            child: Container(
              child: Visibility(
                visible: dinossauro ?? true,
                child: Hero(
                  tag: dinossauro == true ? 'dino' : '',
                  child: Image.asset(
                    caminhoImg ?? "assets/logos/dinossauro.png",
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 15,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 65,
                  child: Container(
                    child: Center(
                      child: IconButton(
                        icon: Icon(
                          iconeDireito,
                          color: Cores.azul75,
                          size: largura * 0.08,
                        ),
                        onPressed: () {
                          if (iconeDireito == Icons.shopping_cart) {
                            Modular.to.pushNamed('/dashboard/cart');
                          } else {
                            funcaoDireita.call();
                          }
                        },
                      ),
                    ),
                  ),
                ),
                Spacer(flex: 35),
              ],
            ),
          )
        ],
      ),
    );
  }
}
