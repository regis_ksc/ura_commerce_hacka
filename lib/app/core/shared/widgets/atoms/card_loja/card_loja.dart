import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';

class CardLoja extends StatelessWidget {
  final String label;
  final String caminho;

  const CardLoja({Key key, this.label = "", this.caminho}) : super(key: key);

  @override
  Widget build(BuildContext context) {}
}
