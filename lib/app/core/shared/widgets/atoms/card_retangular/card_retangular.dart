import 'package:flutter/material.dart';
import 'package:ura_commerce_hacka/app/core/shared/constants/colors.dart';

class CardRetangular extends StatelessWidget {
  final int proporcao;
  final String label;
  final String caminho;
  final Function onTap;

  const CardRetangular({Key key, this.proporcao = 1, this.label = "", this.caminho, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double largura = MediaQuery.of(context).size.width - MediaQuery.of(context).size.width * 0.1;
    final double altura = MediaQuery.of(context).size.height;
    final double larguraPorCento = largura / 100;
    final double alturaPorCento = altura / 100;
    final double lado = largura / 5;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          width: lado * proporcao,
          margin: EdgeInsets.symmetric(
            vertical: larguraPorCento * 2,
            horizontal: larguraPorCento * 2,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Card(
                lado: lado,
                proporcao: proporcao,
                alturaPorCento: alturaPorCento,
                caminho: caminho,
                onTap: onTap,
              ),
              Text(
                label,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Card extends StatelessWidget {
  const Card({
    Key key,
    @required this.lado,
    @required this.proporcao,
    @required this.alturaPorCento,
    @required this.caminho,
    @required this.onTap,
  }) : super(key: key);

  final Function onTap;
  final double lado;
  final int proporcao;
  final double alturaPorCento;
  final String caminho;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      splashColor: Cores.azul100.withOpacity(0.1),
      borderRadius: BorderRadius.circular(10),
      child: Container(
        width: lado * proporcao,
        height: lado * proporcao,
        margin: EdgeInsets.symmetric(vertical: alturaPorCento * 0.5),
        decoration: BoxDecoration(
          color: Cores.amarelo50,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: lado / 100 * 5,
            horizontal: lado / 100 * 5,
          ),
          child: SizedBox.expand(
            child: Image.asset(
              caminho,
            ),
          ),
        ),
      ),
    );
  }
}
