import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  final Color backgroundColor;
  final Color foregroundColor;
  final Color splashColor;
  final double borderRadius;
  final double height;
  final double width;
  final Function onTap;
  final Alignment alignment;
  final Widget child;
  final EdgeInsets padding;
  final EdgeInsets margin;

  const ButtonWidget(
      {Key key,
      @required this.child,
      this.backgroundColor,
      this.foregroundColor,
      this.borderRadius,
      this.height,
      this.onTap,
      this.padding,
      this.width,
      this.alignment,
      this.splashColor,
      this.margin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Material(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 0),
        ),
        color: Colors.transparent,
        child: Card(
          elevation: 3,
          margin: margin ?? EdgeInsets.zero,
          color: backgroundColor ?? Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 0),
          ),
          child: InkWell(
            splashColor: splashColor,
            borderRadius: BorderRadius.circular(borderRadius ?? 0),
            onTap: onTap,
            child: Container(
              padding: padding ??
                  EdgeInsets.symmetric(
                    vertical: 0,
                    horizontal: 0,
                  ),
              height: height ?? 50,
              width: width,
              alignment: alignment ?? Alignment.center,
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}
