import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Fontes {
  Fontes._();

  static const TextStyle nunito = TextStyle(
    fontFamily: 'Nunito',
    decoration: TextDecoration.none,
  );

  static const TextStyle montserrat = TextStyle(
    fontFamily: 'Montserrat',
    decoration: TextDecoration.none,
  );
}
