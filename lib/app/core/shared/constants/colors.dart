import 'package:flutter/material.dart';

/// PARA COLOCAR CORES
/// o padrão de declaração é:
/// static const Color nome = Color(0XFF000000);
///  OU
/// static const Color nome = Color.white;
///

class Cores {
  Cores._();

  // AMARELO
  static const Color amarelo100 = Color(0XFFFFCD3C); // 100% opaco
  static const Color amarelo75 = Color(0XBFFFCD3C); // 75% opaco
  static const Color amarelo50 = Color(0X80FFCD3C); // 50% opaco

  // AZUL
  static const Color azul100 = Color(0XFF162447);
  static const Color azul75 = Color(0XBF162447);
  static const Color azul70 = Color(0XB3162447);
  static const Color azul50 = Color(0X80162447);
  static const Color azul40 = Color(0X66162447);

  // VERMELHO
  static const Color vermelho100 = Color(0XFF900D0D);
  static const Color vermelho75 = Color(0XBF900D0D);

  //TRANSPARENTE
  static const Color transparente = Color(0X00000000);
}
