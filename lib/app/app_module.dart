import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/components/menu_components/pages/cupons/cupons_controller.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/dashboard_controller.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/dashboard_module.dart';
import 'package:ura_commerce_hacka/app/modules/dashboard/modules/shop/shop_page.dart';
import 'package:ura_commerce_hacka/app/modules/onboarding/onboarding_controller.dart';
import 'package:ura_commerce_hacka/app/modules/splashscreen/splashscreen_page.dart';

import 'app_controller.dart';
import 'app_widget.dart';
import 'core/shared/widgets/atoms/barra_de_pesquisa/barra_de_pesquisa_controller.dart';
import 'modules/dashboard/components/menu_components/pages/help/help_controller.dart';
import 'modules/dashboard/components/menu_components/pages/orders/orders_controller.dart';
import 'modules/dashboard/components/menu_components/pages/profile/profile_controller.dart';
import 'modules/dashboard/components/menu_components/pages/sales/sales_controller.dart';
import 'modules/dashboard/modules/shop/shop_controller.dart';
import 'modules/onboarding/onboarding_module.dart';
import 'modules/splashscreen/splashscreen_controller.dart';
import 'repositories/interfaces/login_repository_interface.dart';
import 'repositories/interfaces/register_repository_interface.dart';
import 'repositories/login_repository.dart';
import 'repositories/register_repository.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind<IRegisterRepository>((i) => RegisterRepository(Dio())),
        Bind<ILoginRepository>((i) => LoginRepository(Dio())),
        Bind((i) => AppController()),
        Bind((i) => BarraDePesquisaController()),
        Bind((i) => SplashscreenController()),
        Bind((i) => AppController()),
        Bind((i) => DashboardController()),
        Bind((i) => OnboardingController()),
        Bind((i) => SalesController()),
        Bind((i) => OrdersController()),
        Bind((i) => HelpController()),
        Bind((i) => ProfileController()),
        Bind((i) => CuponsController()),
        Bind((i) => ShopController()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => SplashscreenPage()), // ! TROCAR AQUI
        Router(
          '/onboarding',
          module: OnboardingModule(),
          transition: TransitionType.rightToLeftWithFade,
        ),
        Router(
          '/dashboard',
          // child: (context, args) => OrdersPage(),
          module: DashboardModule(),
          transition: TransitionType.fadeIn,
        ),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
