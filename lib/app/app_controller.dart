import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:mobx/mobx.dart';

part 'app_controller.g.dart';

class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase extends Disposable with Store {
  dispose() {}
  _AppControllerBase() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setKeyBoardIsShown(visible);
        print("o teclado está: ${keyboardIsShown ? 'visível' : 'invisível'}");
      },
    );
  }

  @observable
  FocusScopeNode currentFocus;
  @action

  /// Pass FocusScope.of(context)
  setCurrentFocus(FocusScopeNode focusScope) {
    currentFocus = focusScope;
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      currentFocus.focusedChild.unfocus();
    }
  }

  @observable
  bool keyboardIsShown = false;
  @action
  setKeyBoardIsShown(bool value) {
    keyboardIsShown = value;
  }

  @observable
  bool dialogIsOpen = false;
  @action
  setIfDialogIsOpen(bool value) {
    dialogIsOpen = value;
  }
}
